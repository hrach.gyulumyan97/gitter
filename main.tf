# Configure the AWS Provider
provider "aws" {
 region = "us-east-2"
}
 
# Create an Instance
resource "aws_instance" "my-instance" {
 ami = "ami-0fb653ca2d3203ac1"                   
 instance_type = "t2.micro"
 vpc_security_group_ids = [aws_security_group.web-server.id]
 
 user_data = file("user_data.sh")
}
 
# Create a security group
resource "aws_security_group" "web-server" {
 name        = "WebServer SG"
 description = "Allows inbound traffic"
 
 ingress {
   from_port        = 80
   to_port          = 80
   protocol         = "tcp"
   cidr_blocks      = ["0.0.0.0/0"]
 }
 
 ingress {
   from_port        = 443
   to_port          = 443
   protocol         = "tcp"
   cidr_blocks      = ["0.0.0.0/0"]
 }
 
 egress {
   from_port        = 0
   to_port          = 0
   protocol         = "-1"
   cidr_blocks      = ["0.0.0.0/0"]
 }
 
 tags = {
   Name = "Web Server SG"
 }
}
